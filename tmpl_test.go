package genelet;

import (
	"testing"
	"regexp"
)

func TestTmpl(t *testing.T) {
    configure := New_Config(filename)
	tmpl := new(Tmpl)
    tmpl.File = "tmpl.html"
    tmpl.Other = map[string]interface{}{"Errorstr":Err(1001).Error(), "Script_name":configure.Script_name, configure.Role_name:"m", configure.Go_uri_name:"aaa", "Login_name":"email", "Password_name":"passwd"}
    str := ""
    err := tmpl.Get_page(&str)
	if (err != nil) {
		t.Errorf("%s error", err.Error())
	}
    matched, err := regexp.MatchString("Google authorization required", str)
    if !matched {
        t.Errorf("%s wanted", str)
    }
    matched, err = regexp.MatchString("aaa.*bb.*email.*passwd", str)
    if !matched {
        t.Errorf("%s wanted", str)
    }
}
