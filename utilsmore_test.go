package genelet;

import (
    "testing"
)

func TestGeneletUtils(t *testing.T) {
    str := "abcdefg_+hijk=="
	new_str := Stripchars("df+=", str)
	if "abceg_hijk" != new_str {
		t.Errorf("%s %s wanted", str, new_str)
	}
	//join1 := Joinstrings("|", str, new_str)
	//if "abcdefg_+hijk==|abceg_hijk" != join1 {
//		t.Errorf("%s wanted", join1)
	//}
	x := []string{str, new_str, "abc"}
	if Grep("abcZ", x) {
		t.Errorf("%s wrong matched", "abcZ")
	}
	if Grep("abc", x)==false {
		t.Errorf("%s matched", "abc")
	}
}
