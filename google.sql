CREATE TABLE `goole_operation` (
  `operation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(32) NOT NULL,
  PRIMARY KEY (`operation_id`),
  UNIQUE KEY `email` (`email`(16))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELIMITER ~
DROP PROCEDURE IF EXISTS login_google_a;
CREATE PROCEDURE `login_google_a`(IN in_email VARCHAR(32), OUT o_operation_id INT UNSIGNED)
BEGIN
	SELECT operation_id INTO o_operation_id FROM goole_operation WHERE (email = in_email);
END~
DELIMITER ;
