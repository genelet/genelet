package genelet

import (
//	"log"
	"fmt"
	"net/url"
	"database/sql"
	"encoding/json"
)

type Oauth2 struct {
	Procedure
	Default_pars map[string]string
	Access_token string
}

func New_Oauth2(base Base, db *sql.DB, uri string, provider string) *Oauth2 {
	a := new(Oauth2)
	a.CGI = a
	a.Base = base
	a.DBH = db
	a.Uri = uri
	a.Provider = provider
	a.Default_pars = make(map[string]string)
	switch provider {
	case "google":
	a.Default_pars["scope"]          = "profile"
	a.Default_pars["response_type"]  = "code"
	a.Default_pars["grant_type"]     = "authorization_code"
	a.Default_pars["authorize_url"]  = "https://accounts.google.com/o/oauth2/auth"
	a.Default_pars["access_token_url"] = "https://accounts.google.com/o/oauth2/token"
	a.Default_pars["endpoint"]       = "https://www.googleapis.com/oauth2/v1/userinfo"
	case "facebook":
	a.Default_pars["scope"]          = "public_profile%20email"
	a.Default_pars["authorize_url"]  = "https://www.facebook.com/dialog/oauth"
	a.Default_pars["access_token_url"] = "https://graph.facebook.com/oauth/access_token"
	a.Default_pars["endpoint"]       = "https://graph.facebook.com/me"
	a.Default_pars["fields"]         = "id,email,name,first_name,last_name,age_range,gender"
	case "qq":
	a.Default_pars["scope"]          = "get_user_info"
	a.Default_pars["authorize_url"]  = "https://graph.qq.com/oauth2.0/authorize"
	a.Default_pars["access_token_url"] = "https://graph.qq.com/oauth2.0/token"
	a.Default_pars["grant_type"]     = "authorization_code"
	a.Default_pars["endpoint"]       = "https://graph.qq.com/user/get_user_info"
	a.Default_pars["fields"]         = "nickname, gender"
	case "microsoft":
	a.Default_pars["response_type"]  = "code"
	a.Default_pars["scope"]          = "wl.basic%20wl.offline_access%20wl.emails%20wl.skydrive"
	a.Default_pars["authorize_url"]  = "https://oauth.live.com/authorize"
	a.Default_pars["access_token_url"] = "https://oauth.live.com/token"
	a.Default_pars["grant_type"]     = "authorization_code"
	a.Default_pars["token_method_get"] = "1"
	a.Default_pars["endpoint"]       = "https://apis.live.net/v5.0/me"
	case "salesforce":
	a.Default_pars["response_type"]  = "code"
	a.Default_pars["grant_typ"]      = "authorization_code"
	a.Default_pars["authorize_url"]  = "https://login.salesforce.com/services/oauth2/authorize"
	a.Default_pars["access_token_url"] = "https://login.salesforce.com/services/oauth2/token"
	a.Default_pars["endpoint"]       = "https://login.salesforce.com/id/"
	}

	role := base.C.Roles[base.Role_value]
	issuer := role.Issuers[provider]
	for k, v := range issuer.Provider_pars {
		a.Default_pars[k] = v
	}

	return a
}

func (self *Oauth2) Authenticate(login, password, uri string) error {
	hash := self.Default_pars
	cbk := self.Callback_address(uri)
	if (login == "") {
		if (password != "") { return Err(400) }
		dest := hash["authorize_url"] + "?client_id=" + hash["client_id"] + "&redirect_uri=" + url.QueryEscape(cbk)
		for _, k := range []string{"scope", "display", "state", "response_type"} {
			if v, ok := hash[k]; ok {
				dest += "&" + k + "=" + v
			}
		}
		if hash["access_type"] != "" {
			dest += "&access_type=" + hash["access_type"]
		}
		if hash["approval_prompt"] != "" {
			dest += "&approval_prompt=" + hash["approval_prompt"]
		}

		return Err(303, dest)
	}

	form := make(url.Values)
	form.Set("code", login)
	form.Set("client_id", hash["client_id"])
	form.Set("client_secret", hash["client_secret"])
	form.Set("redirect_uri", cbk)
	if (hash["grant_type"] != "") {
		form.Set("grant_type", hash["grant_type"])
	}
//log.Printf("1 %#v\n", form);

	body := make([]byte,0)
	var err error
	_, ok := hash["token_method_get"]
	if ok {
		body, err =  Get(hash["access_token_url"], form)
		if (err != nil) { return err }
	} else {
		body, err = Post(hash["access_token_url"], form)
		if (err != nil) { return err }
	}

//log.Printf("2 %#v\n", body);
	back := make(map[string]interface{})
	switch self.Provider {
	case "facebook":
		m, err := url.ParseQuery(string(body))
		if (err != nil) { return Err(1400) }
//log.Printf("200 %#v\n", m);
		back["access_token"] = m.Get("access_token")
		back["expires"] = m.Get("expires")
	default:
		err := json.Unmarshal(body, &back)
		if (err != nil) { return err }
	}
	access_token, ok := back["access_token"]
	if !ok { return Err(1401) }
	self.Access_token = access_token.(string)
//log.Printf("3 %#v\n", back);

	form = make(url.Values)
	for k, v := range back {
		if k == "access_token" { continue }
		switch v.(type) {
		case int, int32, int64, uint, uint32, uint64:
			form.Set(k, fmt.Sprintf("%d", v))
		case float32, float64:
			form.Set(k, fmt.Sprintf("%.2f", v))
		default:
			form.Set(k, v.(string))
		}
	}
//log.Printf("4 %#v\n", form);

	if (hash["endpoint"] != "") {
		uri := hash["endpoint"]
		if self.Provider == "salesforce" { uri = form.Get("id") }
		if self.Provider == "facebook" { form.Set("fields", hash["fields"]) }
		back1, err := self.Oauth2_api("GET", uri, form)
		if err != nil { return err }
		for k, v := range back1 {
			back[k] = v
		}
	}
	for k, v := range hash {
		back[k] = v
	}
//log.Printf("5 %#v\n", back);
//log.Printf("50 %#v\n", uri);

	return self.Fill_provider(back, uri)
}

func (self *Oauth2) oauth2_request(method string, uri string, form url.Values) ([]byte, error) {
    hash := self.Default_pars
	if hash["grant_type"]=="authorization_code" {
		h := make(map[string]string)
		h["Authorization"] = "Bearer " + self.Access_token
        return Do(method, uri, form, h)
	}

	if form==nil { form = make(url.Values) }
	form.Set("access_token", self.Access_token)
	return Do(method, uri, form, nil)
}

func (self *Oauth2) Oauth2_api(method string, uri string, form url.Values) (map[string]interface{}, error) {
	body, err := self.oauth2_request(method, uri, form)
	if err != nil { return nil, err }

	return To_hash(body)
}

func (self *Oauth2) Oauth2_apis(method string, uri string, form url.Values) ([]map[string]interface{}, error) {
	body, err := self.oauth2_request(method, uri, form)
	if err != nil { return nil, err }

	return To_slice(body)
}

func (self *Oauth2) multipart_request(uri string, form url.Values, paramName, path string) ([]byte, error) {
    hash := self.Default_pars
	if hash["grant_type"]=="authorization_code" {
		h := make(map[string]string)
		h["Authorization"] = "Bearer " + self.Access_token
        return MultipartUpload(uri, form, h, paramName, path)
	}

	if form==nil { form = make(url.Values) }
	form.Set("access_token", self.Access_token)
	return MultipartUpload(uri, form, nil, paramName, path)
}

func (self *Oauth2) Multipart_upload(uri string, form url.Values, paramName, path string) (map[string]interface{}, error) {
	body, err := self.multipart_request(uri, form, paramName, path)
	if err != nil { return nil, err }

	return To_hash(body)
}

func (self *Oauth2) Multipart_uploads(uri string, form url.Values, paramName, path string) ([]map[string]interface{}, error) {
	body, err := self.multipart_request(uri, form, paramName, path)
	if err != nil { return nil, err }

	return To_slice(body)
}
