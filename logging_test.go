package genelet;

import (
	"fmt"
	"os"
	"bufio"
	"testing"
)

const (
	fn = "logging.t"
	MSG1 = "__msg1__"
	MSG2 = "__msg2__"
	MSG3 = "__msg3__"
)

func get_last(l *Logging) string {
	file, err := os.Open(l.Filename)
	if err != nil {
		panic(err.Error())
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var tmp string
	for scanner.Scan() {
		tmp = fmt.Sprint(scanner.Text())
		//fmt.Println(scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading file:", err)
	}
	return tmp
	//return scanner.Text()
}

func TestLog(t *testing.T) {
	var filename = fmt.Sprint(os.TempDir(), "/", fn)
//fmt.Fprintf(os.Stderr, "%s\n", os.TempDir())
//fmt.Fprintf(os.Stderr, "%s\n", filename)
	logger := New_Logging(filename, "Emergency", "Info")
	if logger.Filename != filename {
		t.Errorf("%s wanted", filename)
	}
	if logger.Minlevel != "Emergency" {
		t.Errorf("Emergency wanted")
	}
	if logger.Maxlevel != "Info" {
		t.Errorf("Info wanted")
	}
	if logger.Is_emergency()!=true {
		t.Errorf("Emergency will be logged")
	}
	if logger.Is_critical()!=true {
		t.Errorf("Critial will be logged")
	}
	if logger.Is_info() != true {
		t.Errorf("Info will be logged")
	}
	if logger.Is_debug() == true {
		t.Errorf("Debug will NOT be logged")
	}

	logger.Emergency(MSG1)
	in := get_last(logger)
	wanted := fmt.Sprintf("logging.go:51: [Emergency %d]%s", os.Getpid(), MSG1)
	if in != wanted {
		t.Errorf("Actual: %s expected: %s", in, wanted)
    }
	logger.Critical(MSG2)
	in = get_last(logger)
	wanted = fmt.Sprintf("logging.go:51: [Critical %d]%s", os.Getpid(), MSG2)
	if in != wanted {
		t.Errorf("Actual: %s expected: %s", in, wanted)
    }
	logger.Info(MSG3)
	in = get_last(logger)
	wanted = fmt.Sprintf("logging.go:51: [Info %d]%s", os.Getpid(), MSG3)
	if in != wanted {
		t.Errorf("Actual: %s expected: %s", in, wanted)
    }
	logger.Debug(MSG3)
	in = get_last(logger)
	wanted = fmt.Sprintf("logging.go:51: [Debug %d]%s", os.Getpid(), MSG3)
	if in == wanted {
		t.Errorf("Actual: %s NOT expected: %s", in, wanted)
    }

	logger.Maxlevel = "Debug"
	if logger.Is_debug() != true {
		t.Errorf("Debug will be logged")
	}
	logger.Debug(MSG3)
	in = get_last(logger)
	wanted = fmt.Sprintf("logging.go:51: [Debug %d]%s", os.Getpid(), MSG3)
	if in != wanted {
		t.Errorf("Actual: %s expected: %s", in, wanted)
    }

	err := os.Remove(filename)
	if err != nil {
		panic(err.Error())
    }
}
