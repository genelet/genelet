package genelet

import (
	"net/url"
)

type Filter struct {
	Base
	Action		string
	Component	string
	Actions		map[string]map[string][]string
	Fks			map[string][]string
	OTHER		*map[string]interface{}
}

func (self *Filter)Set_all(base Base, action string, component string, other *map[string]interface{}) {
	self.Base = base
	self.Action = action
	self.Component = component
	self.OTHER = other
}

func (self *Filter)Get_all() (map[string][]string, []string) {
	actionHash, found := self.Actions[self.Action]
	if (!found) {
		return nil, nil
	}

	if (self.Fks == nil) {
		return actionHash, nil
	}
	fk, found := self.Fks[self.Role_value]
	if (found) {
		return actionHash, fk
	}
	return actionHash, nil
}

func (self *Filter)Get_gate(role_value ...string) *Gate {
	base := self.Base
	if (role_value != nil && role_value[0] != base.Role_value) {
		base = Base{C:base.C, W:base.W, R:base.R, Role_value:role_value[0], Chartag_value:base.Chartag_value}
	}
	return New_Gate(base)
}

func (self *Filter)Preset() error {
    return nil
}

func (self *Filter)Before(model *Model, extra url.Values, nextextra url.Values)  error {
    return nil
}

func (self *Filter)After(model *Model) error {
    return nil
}

