package genelet

import (
	"net/url"
	"bytes"
	"text/template"
)

type Tmpl struct {
	File	string
	Dir		string
	Lists	[]map[string]interface{}
	ARGS	url.Values
	Other	map[string]interface{}
	Extra	map[string]interface{}
	Success	bool
}

func (self *Tmpl)Get_page(res *string, T ...*template.Template) error {
	var buffer bytes.Buffer
	var err error
	if (T == nil) {
		T0, err := template.ParseFiles(self.File)
		if (err != nil) { return err }
		if (self.Dir != "") {
			T0 = template.Must(T0.ParseGlob(self.Dir+"/*"))
		}
		err = T0.Execute(&buffer, *self)
	} else {
		err = T[0].Execute(&buffer, *self)
	}
	if (err != nil) { return err }
	*res = buffer.String()
	return nil
}
