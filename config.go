package genelet

import (
	"regexp"
	"encoding/json"
	"io/ioutil"
)

type Issuer struct {
	Default	bool
	Screen	int8
	Sql		string
	Sql_as	string
	Provider_pars	map[string]string
	Credential	[]string
	In_pars		[]string
	Out_pars	[]string
}

type Role struct {
	Id_name			string
	Type_id			int
	Is_admin		bool
	Attributes		[]string

	Coding  string
	Secret  string
	Surface string
	Length  int8
	Duration    int
	Userlist    []string
	Grouplist   []string
	Logout  string
	Domain  string
	Path    string
	Max_age int

	Issuers	map[string]Issuer
}

type Config struct {
	Uploaddir	string
	Template	string
	Temp_name	string
	Pubrole		string
	Secret		string
	Document_root	string
	Script_name	string
	Action_name	string
	Default_actions	map[string]string
	Role_name	string
	Plain_provider	string
	Google_provider	string
	Facebook_provider	string
	Qq_provider	string
	Twitter_provider	string
	Linkedin_provider	string
	Login_name	string
	Logout_name	string
	Tag_name	string
	Provider_name	string
	Callback_name	string
	Go_uri_name		string
	Go_probe_name   string
	Go_err_name string

	Db			[]string
	Blks		map[string]map[string]string
	Chartags    map[string]Chartag
	Roles       map[string]Role
	Errors		map[string]string
	Log			map[string]string
	Patterns	[][]string
	Static		[][2]interface{}
}

func New_Config(filename string) Config {
	var parsed Config
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(content, &parsed)
	if err != nil {
		panic(err)
	}
	if (parsed.Uploaddir=="") {
		parsed.Uploaddir = "/tmp"
	}
	if (parsed.Action_name=="") {
		parsed.Action_name = "action"
	}
	if (parsed.Go_uri_name=="") {
		parsed.Go_uri_name = "go_uri"
	}
	if (parsed.Role_name=="") {
		parsed.Role_name = "role"
	}
	if (parsed.Plain_provider=="") {
		parsed.Plain_provider = "plain"
	}
	if (parsed.Google_provider=="") {
		parsed.Google_provider= "google"
	}
	if (parsed.Facebook_provider=="") {
		parsed.Facebook_provider= "facebook"
	}
	if (parsed.Qq_provider=="") {
		parsed.Qq_provider= "qq"
	}
	if (parsed.Twitter_provider=="") {
		parsed.Twitter_provider= "twitter"
	}
	if (parsed.Linkedin_provider=="") {
		parsed.Linkedin_provider= "linkedin"
	}
	if (parsed.Login_name=="") {
		parsed.Login_name = "login"
	}
	if (parsed.Logout_name=="") {
		parsed.Logout_name = "logout"
	}
	if (parsed.Tag_name=="") {
		parsed.Tag_name = "tag"
	}
	if (parsed.Provider_name=="") {
		parsed.Provider_name = "provider"
	}
	if (parsed.Callback_name=="") {
		parsed.Callback_name = "callback"
	}
	if (parsed.Go_probe_name=="") {
		parsed.Go_probe_name = "go_probe"
	}
	if (parsed.Go_err_name=="") {
		parsed.Go_err_name = "go_err"
	}
	if (parsed.Errors == nil) {
		parsed.Errors  = make(map[string]string)
	}

	if parsed.Default_actions == nil {
        parsed.Default_actions = map[string]string{"GET":"dashboard", "GET_item":"edit", "PUT":"update", "POST":"insert", "DELETE":"delete"}
    }

	for i, fields := range parsed.Patterns {
		reg := regexp.MustCompile(fields[0])
		parsed.Static[i] = [2]interface{}{reg, fields[1:]}
	}

	return parsed
}

func (self *Config) Error_string(e error) string {
	re := regexp.MustCompile("^\\d+$")
	x  := re.FindAllString(e.Error(),1)
	if (x == nil) {
		return e.Error()
	} else {
		elem, ok := self.Errors[x[0]]
		if ok {
			return x[0] + ": " + elem
		} else {
			return x[0]
		}
	}
	return ""
}
