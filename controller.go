package genelet

import (
	"bufio"
	"io"
	"regexp"
	"strings"
	"log"
	"fmt"
	"os"
	"io/ioutil"
	"strconv"
	"database/sql"
	"encoding/json"
	"net/url"
	"net/http"
	_ "github.com/go-sql-driver/mysql"
)

type Controller struct {
	C	Config
	Models	map[string]interface{}
	Filters	map[string]interface{}
	Storage	map[string]interface{}
	Toy	bool
}

func (self *Controller)static_page(w http.ResponseWriter, r *http.Request) {
	if (strings.Contains(r.URL.Path, `..`)) {
		http.NotFound(w,r)
	}
log.Printf("(%d) %s\n", os.Getpid(), "not genelet pattern")
	url := r.URL.Path
	form := r.Form

	for _, fields := range self.C.Static {
		values := fields[0].(*regexp.Regexp).FindStringSubmatch(url)
		if (values == nil) { continue }
		for i, f := range fields[1:] {
			form.Set(f.(string), values[i])
		}
		if (form.Get(self.C.Role_name)=="" || form.Get(self.C.Role_name)==self.C.Pubrole) {
			break
		}
		if (form.Get(self.C.Tag_name)=="") {
log.Printf("(%d) %s\n", os.Getpid(), "static file matched not good")
			http.NotFound(w,r)
			return
		}
		base := &Base{C:self.C, R:r, W:w, Role_value:form.Get(self.C.Role_name), Chartag_value:form.Get(self.C.Tag_name)}
		gate := New_Gate(*base)
		err := gate.Forbid()
		if (err != nil) {
log.Printf("(%d) %s\n", os.Getpid(), "static file asks for login")
			self.login_page(base)
			return
		}
		break
	}

	http.ServeFile(w, r, self.C.Document_root+r.URL.Path)
}

func (self *Controller)login_page(base *Base) {
	c := self.C
	uri := base.R.Form.Get(c.Go_uri_name)

	var err error
	if (self.Toy) {
log.Printf("(%d) %s\n", os.Getpid(), "provider plain")
		ticket := New_Ticket(*base, uri, c.Plain_provider)
		err = ticket.Handler()
	} else {
		provider := base.R.Form.Get(c.Provider_name)
log.Printf("(%d) %s %s\n", os.Getpid(), "provider", provider)
		if (provider == "") {
			provider = base.Get_provider()
			if (provider == "") {
				http.NotFound(base.W, base.R)
				return
			}
		}
		db, err1 := sql.Open(c.Db[0], c.Db[1])
		defer db.Close()
		if (err1 != nil) {
			http.Error(base.W, "Database error", 500)
			return
		}
log.Printf("(%d) %s %s\n", os.Getpid(), "ticket", provider)
		if (provider == c.Google_provider || provider == c.Facebook_provider || provider == c.Qq_provider) {
			ticket := New_Oauth2(*base, db, uri, provider)
			err = ticket.Handler_login()
		} else if (provider == c.Twitter_provider || provider == c.Linkedin_provider) {
			ticket := New_Oauth1(*base, db, uri, provider)
			err = ticket.Handler_login()
		} else {
			ticket := New_Procedure(*base, db, uri, provider)
			err = ticket.Handler()
		}
	}
	if (err != nil) {
		base.Send_status_page(err.(Gerror).Code, err.(Gerror).Errstr)
	}
}

func check_form(r *http.Request, dir string) error {
	reader, err := r.MultipartReader()

	if reader == nil {
		err = r.ParseForm()
		if (err != nil) { return err }
		form := r.Form

		header := r.Header
		// not json, return
		if (header.Get("Content-Type") == "" || !strings.Contains(header.Get("Content-Type"), "application/json") || r.Body == nil) {
			return nil
		}

		data, err := ioutil.ReadAll(r.Body)
		t := make(map[string]interface{})
		if err == nil && data != nil {
			err = json.Unmarshal(data, &t)
		}
		if err != nil { return err }

		for key, value := range t {
			if (value==nil) { continue; }
			switch value.(type) {
			case []string:
				for _, v := range value.([]string) {
					form.Add(key, v)
				}
			case []int:
				for _, v := range value.([]int) {
					form.Add(key, strconv.Itoa(v))
				}
			case int:
				form.Add(key, strconv.Itoa(value.(int)))
			case int32:
				form.Add(key, strconv.Itoa(int(value.(int32))))
			case int64:
				form.Add(key, strconv.Itoa(int(value.(int64))))
			case float64:
				form.Add(key, strconv.FormatFloat(value.(float64), 'f', 6, 64))
			case string:
				form.Add(key, value.(string))
			default:
				form.Add(key, value.(string))
			}
		}
	} else {
		r.Form = make(url.Values)
		form := r.Form
		for {
			part, err := reader.NextPart()
			if err == io.EOF { break }

			field_name := part.FormName()
			file_name  := part.FileName()
			if file_name == "" {
				scanner := bufio.NewScanner(part)
				scanner.Scan()
				form.Add(field_name, scanner.Text())
			} else {
				fullname := dir + "/" + file_name
				dst, err := os.Create(fullname)
				defer dst.Close()
				if err != nil { return err }
				if _, err := io.Copy(dst, part); err != nil { return err }
				form.Add(field_name, file_name)
			}
			part.Close()
		}
	}


	return nil
}

func (self *Controller)ServeHTTP(w http.ResponseWriter, r *http.Request) {
	c := self.C
	length := len(c.Script_name)
	l_url  := len(r.URL.Path)

	if origin := r.Header.Get("Origin"); origin != "" {
		w.Header().Set("Access-Control-Allow-Origin", origin)
	}
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "x-requested-with, Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	if r.Method == "OPTIONS" {
		w.WriteHeader(200)
		return
	}

log.Printf("(%d) start url: %s\n", os.Getpid(), r.URL.Path)
	if (l_url <= length || r.URL.Path[:length+1] != c.Script_name+"/") {
		self.static_page(w, r)
		return
	}

	method_found := false
	for k, _ := range c.Default_actions {
		if k == r.Method {
			method_found = true
			break
		}
	}
	if method_found == false {
		http.Error(w, "The http method is not supported", 405)
		return
	}

	path_info := strings.Split(r.URL.Path[length+1:], "/")
	if (len(path_info) == 4) {
		r.Header.Add("X-Forwarded-ID", path_info[3])
	} else if (len(path_info) != 3) {
log.Printf("(%d) %s\n", os.Getpid(), "not genelet url")
		http.Error(w, "Bad Request", 400)
		return
	}

	chartag, ok := c.Chartags[path_info[1]]
	if (!ok) {
log.Printf("(%d) %s\n", os.Getpid(), "check chartag")
		http.Error(w, "Bad Request", 400)
		return
	}

	base := &Base{C:c, W:w, R:r, Role_value:path_info[0], Chartag_value:path_info[1]}
	gate := New_Gate(*base)
	obj  := path_info[2]

log.Printf("(%d) %s\n", os.Getpid(), "parse form")
	err := check_form(r, c.Uploaddir)
	if (err != nil) {
		http.Error(w, "Bad Request", 400)
		return
	}

	if (c.Log != nil) {
log.Printf("(%d) %s\n", os.Getpid(), "logger starts")
		base.Logger = New_Logging(c.Log["Filename"], c.Log["Minlevel"], c.Log["Maxlevel"])
	}

log.Printf("(%d) %s\n", os.Getpid(), "if role is defined or public")
	_, ok = c.Roles[base.Role_value]
	if (!ok && (gate.Role_value != c.Pubrole)) {
		http.NotFound(w,r)
		return
	}

log.Printf("(%d) %s %s\n", os.Getpid(), "object is", obj)
	if (obj == c.Login_name || obj == c.Google_provider || obj == c.Facebook_provider || obj == c.Qq_provider || obj == c.Twitter_provider || obj == c.Linkedin_provider) {
log.Printf("(%d) %s %s\n", os.Getpid(), "start login for", obj)
		if (obj==c.Google_provider || obj == c.Facebook_provider || obj == c.Qq_provider || obj == c.Twitter_provider || obj == c.Linkedin_provider) {
			r.Form.Set(c.Provider_name, obj)
		}
		self.login_page(base)
log.Printf("(%d) %s\n\n", os.Getpid(), "end login ...")
		return
	} else if (obj == c.Logout_name) {
log.Printf("(%d) %s\n", os.Getpid(), "start logout")
		err = gate.Handler_logout()
		if (err != nil) {
			gate.Send_status_page(err.(Gerror).Code, err.(Gerror).Errstr)
log.Printf("(%d) %s\n\n", os.Getpid(), "end logout ...")
		}
		return
	}

	if (gate.Role_value != c.Pubrole) {
		err = gate.Forbid();
		if (err != nil) {
log.Printf("(%d) forbidden ... %d : %s\n\n", os.Getpid(), err.(Gerror).Code, err.(Gerror).Errstr)
			gate.Send_status_page(err.(Gerror).Code, err.(Gerror).Errstr)
			return
		}
	}

log.Printf("(%d) %s\n", os.Getpid(), "starting ... genelet handler")
	err = self.Handle(obj, *base, r.Method)
log.Printf("(%d) %s\n\n", os.Getpid(), "ending ... genelet handler")
	if (err != nil) {
		switch err.(type) {
		case Gerror:
		if err.(Gerror).Code < 1000 {
			base.Send_status_page(err.(Gerror).Code, add_json(chartag.Case, err.(Gerror).Errstr))
			return
		} else {
			base.Send_page(add_json(chartag.Case, err.Error()))
		}
		default:
			base.Send_page(add_json(chartag.Case, err.Error()))
		}
	}
	return
}

func add_json(c int8, msg string) string {
	if (c>0) {
		return `{"data": "` + msg + `"}`
	}
	return msg
}

func (self *Controller)Handle(obj string, base Base, method string) error {
	model, ok  := self.Models[obj]
	if (!ok) { return Err(404) }
	filter, ok := self.Filters[obj]
	if (!ok) { return Err(404) }
	who  := base.Role_value
	tag  := base.Chartag_value

	c := self.C
	r := base.R
	ARGS := r.Form

	lists := make([]map[string]interface{},0)
	other := make(map[string]interface{})
log.Printf("(%d) %s\n", os.Getpid(), "initialize")
	Invoke0(model, "Initialize")
	Invoke0(model, "Set_defaults", &ARGS, &lists, &other, base.Logger, self.Storage)

	action := ARGS.Get(c.Action_name)
	if (action == "") {
		action = c.Default_actions[method]
		if method=="GET" && r.Header.Get("X-Forwarded-ID") != "" {
			action = c.Default_actions["GET_item"]
		}
	}
	if r.Header.Get("X-Forwarded-ID") != "" {
		ARGS.Set("_gid_url", r.Header.Get("X-Forwarded-ID"))
	}
log.Printf("(%d) %s\n", os.Getpid(), "get action: " + action)
	Invoke0(filter, "Set_all", base, action, obj, &other)
	Invoke0(filter, "Initialize")

	ret := Invoke(filter, "Get_all")
	if (ret[0].Interface() == nil) { return Err(404) }
	actionHash := ret[0].Interface().(map[string][]string)
	fk := make([]string,0)
	if (ret[1].Interface() != nil) {
		fk = ret[1].Interface().([]string)
	}

log.Printf("(%d) %s\n", os.Getpid(), "parsing ARGS")
	parts := strings.Split(r.RequestURI, "/")
	parts[3] = "json"
	ARGS.Set("_guri_json", strings.Join(parts, "/"))
	ARGS.Set("_guri", r.RequestURI)
	ARGS.Set("_grole", who)

	role, ok := c.Roles[base.Role_value]
	is_admin := false
	if (ok) {
log.Printf("(%d) %s\n", os.Getpid(), "parsing role ARGS")
		ARGS.Set("_gid_name", role.Id_name)
		ARGS.Set("_gtype_id", strconv.Itoa(role.Type_id))
		if (role.Is_admin) {
			ARGS.Set("_gadmin", "1")
			is_admin = true
		}
		h := r.Header
		ARGS.Set("_gtime",     h.Get("X-Forwarded-Request_Time"))
		ARGS.Set("_gwhen",     h.Get("X-Forwarded-Time"))
		ARGS.Set("_gduration", h.Get("X-Forwarded-Duration"))
		if h.Get("X-Forwarded-User") == "" || h.Get("X-Forwarded-User") == "NULL" {
			return Err(401)
		}
		ARGS.Set(role.Attributes[0], h.Get("X-Forwarded-User"))
		if (len(role.Attributes)>1) {
			groups := strings.Split(h.Get("X-Forwarded-Group"), "|")
			for i:=0; i<len(groups); i++ {
				ARGS.Set(role.Attributes[i+1], groups[i])
			}
		}
	}

log.Printf("(%d) %s\n", os.Getpid(), "role access control")
	if (!is_admin && base.Role_value != c.Pubrole && !Grep(who, actionHash["groups"])) {
		return Err(401)
	}

	extra := make(url.Values)
	if (!is_admin && ok) {
log.Printf("(%d) %s\n", os.Getpid(), "check fk")
		err := self.assign_fk(who, fk, ARGS, extra)
		if (err != nil) { return err.(error) }
	}

log.Printf("(%d) %s\n", os.Getpid(), "preset")
	err := Invoke(filter, "Preset")[0].Interface()
	if (err != nil) { return err.(error) }

log.Printf("(%d) %s\n", os.Getpid(), "validation")
	validate, ok := actionHash["validate"]
	if (ok) {
		for _, field := range validate {
			if (ARGS.Get(field) == "") { return Err(1092, field) }
		}
	}

	options, ok := actionHash["options"]
	if !ok || !Grep("no_db", options) {
log.Printf("(%d) %s\n", os.Getpid(), "open db")
		dbh, err := sql.Open(c.Db[0], c.Db[1])
		if (err != nil) { return err }
log.Printf("(%d) %s\n", os.Getpid(), "open success")
		Invoke0(model, "Set_dbh", dbh)
		defer dbh.Close()
	}

	nextextra := make(url.Values)
log.Printf("(%d) %s\n", os.Getpid(), "before")
	err = Invoke(filter, "Before", model, extra, nextextra)[0].Interface()
	if (err != nil) { return err.(error) }

	if !ok && !Grep("no_method", options) {
		x:=strings.ToUpper(action[:1])+action[1:]
log.Printf("(%d) %s\n", os.Getpid(), "call model")
		err := Invoke(model, x, extra, nextextra)[0].Interface()
		if (err != nil) { return err.(error) }
	}

	if !is_admin {
log.Printf("(%d) %s\n", os.Getpid(), "fk tobe")
		self.assign_fk_tobe(who, fk, ARGS, lists)
	}

log.Printf("(%d) %s\n", os.Getpid(), "after")
	err = Invoke(filter, "After", model)[0].Interface()
	if (err != nil) { return err.(error) }

log.Printf("(%d) %s\n", os.Getpid(), "call blocks")
	eb := New_Blks(c.Blks).Send(lists, ARGS, other)
	if (eb != nil) { return eb }

	result := &Tmpl{ARGS:ARGS, Lists:lists, Other:other, Success:true}
	chartag := c.Chartags[tag]
	if (chartag.Case > 0) {
log.Printf("(%d) %s\n", os.Getpid(), "generate json")
		b, eb := json.Marshal(result)
		if (eb != nil) { return eb }
			base.Send_page(string(b))
		return nil
	}

log.Printf("(%d) call template\n", os.Getpid())
	if c.Temp_name != "" {
		result.Dir = c.Template + "/" + who + "/" + c.Temp_name
	}
	result.File = c.Template + "/" + who + "/" + obj + "/" + action + "." + tag

	output := ""
log.Printf("(%d) %s\n", os.Getpid(), "generate page")
	eb = result.Get_page(&output)
	if (eb != nil) { return eb }
log.Printf("(%d) %s\n", os.Getpid(), "sending page")
	base.Send_page(output)
	return nil
}

func (self *Controller) assign_fk(who string, fk []string, ARGS url.Values, extra url.Values) error {
	if (fk == nil || self.C.Secret == "") { return nil }
	roleid := ARGS.Get("_gid_name")
	name := fk[0]
	value := ARGS.Get(name)
	if (value == "") { return Err(1041) }
	extra.Set(name, value)
	if (name == roleid) { return nil }

	if (fk[1] == "") { return Err(1054) }
	md5 := ARGS.Get(fk[1])
	if (md5 == "") { return Err(1055) }


	stamp := ARGS.Get("_gwhen")
	value_roleid := ARGS.Get(roleid)
	if md5 != Digest(self.C.Secret, stamp, who, roleid, value_roleid, name, value) {
		return Err(1052)
	}
	if (ARGS.Get("_gduration") != "") {
		gtime, _ := strconv.Atoi(ARGS.Get("_gtime"))
		slast, _ := strconv.Atoi(stamp)
		if (gtime > slast) { return Err(1053) }
	}

	return nil
}

func (self *Controller) fk_tobe(lists []map[string]interface{}, fk []string, stamp, who, roleid, value_roleid string) {
	if (fk[2] == "" || fk[2]==roleid || fk[3]=="") { return }
	name := fk[2]
	for _, item := range lists {
		item_name, ok := item[name]
		if (!ok) { continue }
		var value string
		switch item_name.(type) {
		case int16, uint16, int, uint, int32, uint32, int64, uint64:
			value = fmt.Sprintf("%d", item_name)
		default:
			value = item_name.(string)
		}
		item[fk[3]] = Digest(self.C.Secret, stamp, who, roleid, value_roleid, name, value)
	}
	return
}

func (self *Controller) assign_fk_tobe(who string, fk0 []string, ARGS url.Values, lists []map[string]interface{}) error {
	if (fk0 == nil || self.C.Secret == "") { return nil }
	roleid := ARGS.Get("_gid_name")

	stamp := ARGS.Get("_gwhen")
	value_roleid := ARGS.Get(roleid)

	fk := make([]string, len(fk0))
	copy(fk, fk0)

	self.fk_tobe(lists, fk, stamp, who, roleid, value_roleid)

	for (len(fk)>4) {
		fk = fk[3:]
		which := fk[1]
		if (lists[0][which] == nil) { return Err(1056) }
		for _, item := range lists {
			self.fk_tobe(item[which].([]map[string]interface{}), fk, stamp, who, roleid, value_roleid)
		}
	}
	return nil
}
