package genelet

import (
	"net/url"
)

type Blks struct {
	Mail	*Smtp
	Smail	*Smtpssl
}

func New_Blks(c map[string]map[string]string) *Blks {
  blks := new(Blks)
  for key, value := range c {
    switch key {
    case "mail":
      mail := new(Smtp)
      headers := make(map[string]string)
      for k, v := range value {
         if k=="Username" {
           mail.Username = v
         } else if k=="Password" {
           mail.Password = v
         } else if k=="Address" {
           mail.Address = v
         } else if k=="From" {
           mail.From = v
         } else {
           headers[k] = v
         }
      }
      mail.Headers = headers
      blks.Mail = mail
    case "smail":
      smail := new(Smtpssl)
      headers := make(map[string]string)
      for k, v := range value {
         if k=="Username" {
           smail.Username = v
         } else if k=="Password" {
           smail.Password = v
         } else if k=="Address" {
           smail.Address = v
         } else if k=="From" {
           smail.From = v
         } else {
           headers[k] = v
         }
      }
      smail.Headers = headers
      blks.Smail = smail
    }
  }
  return blks
}

func (self *Blks)Send(lists []map[string]interface{}, ARGS url.Values, other map[string]interface{}) error {

	blocked := map[string]interface{}{"_gmail":self.Mail, "_gsmail":self.Smail}

    for gmail, obj := range blocked {
      env := other[gmail]
      if (obj == nil || env == nil) {
		continue
	  }
      outmail := ""
      envelope := env.(map[string]interface{})
	  if (envelope["content"] != nil) {
		outmail = envelope["content"].(string)
	  } else if (envelope["file"] != nil) {
		var tmpl *Tmpl
		if (envelope["extra"] == nil) {
		tmpl = &Tmpl{envelope["file"].(string), "", lists, ARGS, other, nil, true}
		} else {
		tmpl = &Tmpl{envelope["file"].(string), "", lists, ARGS, other, envelope["extra"].(map[string]interface{}), true}
		}
		err := tmpl.Get_page(&outmail)
        if (err != nil) { return err }
        if (outmail == "") { return Err(1061) }
      }

	  headers := make(map[string]string)
	  for key, val := range envelope {
        if (Grep(key, []string{"file", "content", "callback", "extra"})) {
          continue;
        }
        headers[key] = val.(string)
      }
	  var err error
	  switch obj.(type) {
	  case *Smtp:
		err = self.Mail.Send(headers, outmail)
	  case *Smtpssl:
		err = self.Smail.Send(headers, outmail)
	  }
      if err != nil { return err }
    }

    return nil
}
