package genelet;

import (
    "testing"
)

const (
	filename = "peter.conf"
)

func TestConfig(t *testing.T) {
	c := New_Config(filename)
	if c.Document_root != "aa" {
		t.Errorf("%s wanted", "aa")
	}
	c.Document_root = "root"
	if c.Document_root != "root" {
		t.Errorf("%s wanted", "root")
	}
	if c.Script_name != "bb" {
		t.Errorf("%s wanted", "bb")
	}
	if c.Pubrole != "cc" {
		t.Errorf("%s wanted", "cc")
	}
	if c.Secret != "" {
		t.Errorf("%s is empty", "secret")
	}
	c.Secret = "dd"
	if c.Secret != "dd" {
		t.Errorf("%s wanted", "dd")
	}
    if c.Template != "ee" {
        t.Errorf("%s wanted", "ee")
    }
    if c.Action_name != "action" {
        t.Errorf("%s wanted", "action")
    }
    if c.Go_uri_name != "go_uri" {
        t.Errorf("%s wanted", "go_uri")
    }
	if c.Db[0] != "mysql" {
		t.Errorf("%s wanted", c.Db[0])
	}
	if c.Db[1] != "eightran_goto:12pass34@/wavelet" {
		t.Errorf("%s wanted", c.Db[1])
	}
	if c.Db[2] != "ccc" {
		t.Errorf("%s wanted", "ccc")
	}
	if c.Blks["smail"]["Address"] != "email-smtp.us-west-2.amazonaws.com:465" {
		t.Errorf("%s wanted", "email-smtp.us-west-2.amazonaws.com:465")
	}
	if c.Blks["smail"]["From"] != "peter@greetingland.com" {
		t.Errorf("%s wanted", "peter@greetingland.com")
	}
	char := c.Chartags["json"]
	if char.Content_type != "application/json; charset=\"UTF-8\"" {
		t.Errorf("%s wanted", "application/json; charset=\"UTF-8\"")
	}
	if char.Challenge != "challenge" {
		t.Errorf("%s wanted", "challenge")
	}
    if c.Error_string(Gerror{999,""}) != "999" {
        t.Errorf("Error code 999 should return string 999")
    }
    if c.Error_string(Gerror{2000,""}) != "2000: some1" {
        t.Errorf("Error code 2000 should be some1")
    }
    if c.Error_string(Gerror{1001,""}) != "1001: Google authorization required." {
        t.Errorf("Error code 1001 is wrong")
    }
    if c.Error_string(Gerror{3001,""}) != "3001: some2" {
        t.Errorf("Error code 3001 should be some2")
    }
    if c.Error_string(Gerror{3002,""}) != "3002: some3" {
        t.Errorf("Error code 3002 should be some3")
    }
}
