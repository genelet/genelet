package genelet

import (
	"fmt"
	"strings"
	"strconv"
)

type CGI interface {
	Set_ip() string
	Set_when() int
	Authenticate(string, string, string) error
}

type Access struct {
	Base
	CGI
}

func (self *Access)Set_ip() string {
	ip := ""
	role := self.C.Roles[self.Role_value]
	if (role.Length!=0) {
		full := fmt.Sprintf("%02X%02X%02X%02X", strings.Split(self.Get_ip(), "."))
		ip = full[0:role.Length-1]
	}
	return ip
}

func (self *Access)Set_when() int {
	return Unix_timestamp()
}

func New_Access(base Base) *Access {
	a := new(Access)
	a.CGI = a
	a.Base = base
	return a
}

func (self *Access)Signature(fields ...string) string {
	login := fields[0];
	fields = append(fields[:0], fields[1:]...)
	if len(fields)==0 {
		return self.sign(login, "")
	}
	return self.sign(login, strings.Join(fields, "|"))
}

func (self *Access)sign(login, group string) string {
	role := self.C.Roles[self.Role_value]
	when := strconv.Itoa(self.CGI.Set_when() + role.Duration)
	ip := self.CGI.Set_ip()
	hash := Digest(role.Secret, ip, login, group, when)

	return Encode_scoder(strings.Join([]string{ip, login, group, when, hash}, "/"), role.Coding)
}

func (self *Access)get_cookie(raws ...string) (string, string, string, string, string, error) {
    role, ok := self.C.Roles[self.Role_value]
    if (!ok) { return "", "", "", "", "", Err(1029) }

	raw := ""
	if (raws == nil) {
		coo, err := self.R.Cookie(role.Surface)
		if (err != nil) { return "", "", "", "", "", Err(1030) }
		raw = coo.Value
	} else {
		raw = raws[0]
	}

	value := Decode_scoder(raw, role.Coding)
	x := strings.Split(value, "/");
	if (len(x) < 5) { return "", "", "", "", "", Err(1020) }
	ip, login, group, when, hash := x[0], x[1], x[2], x[3], x[4]
	if (self.CGI.Set_ip() != ip) { return "", "", "", "", "", Err(1023) }
	w, err :=  strconv.Atoi(when)
	if err != nil { return "", "", "", "", "", Err(1026, err.Error()) }
	request_time := self.CGI.Set_when()
	if (request_time>w) { return "", "", "", "", "", Err(1022) }
	if role.Grouplist != nil {
		if Grep(group, role.Grouplist)==false { return "", "", "", "", "", Err(1021) }
	}
	if role.Userlist != nil {
		if Grep(login, role.Userlist)==false { return "", "", "", "", "", Err(1021) }
	}

	if (Digest(role.Secret, ip, login, group, when) != hash) { return "", "", "", "", "", Err(1024) }
	return ip, login, group, when, hash, nil
}

func (self *Access)Verify_cookie(raw string) error {
	_, login, group, when, hash, err := self.get_cookie(raw)
	if err != nil { return err }

	self.R.Header.Add("X-Forwarded-Time", when)
	self.R.Header.Add("X-Forwarded-User", login)
	self.R.Header.Add("X-Forwarded-Group", group)
	self.R.Header.Add("X-Forwarded-Raw", raw)
	self.R.Header.Add("X-Forwarded-Hash", hash)
	role := self.C.Roles[self.Role_value]
	self.R.Header.Add("X-Forwarded-Duration", strconv.Itoa(role.Duration))
	self.R.Header.Add("X-Forwarded-Request_Time", strconv.Itoa(self.CGI.Set_when()))

	return nil
}
