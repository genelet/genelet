package genelet

import (
	"strconv"
	"text/template"
)

type Ticket struct {
	Access
	Uri			string
	Provider	string
	Out_hash	map[string]interface{}
}

func New_Ticket(base Base, uri string, provider string) *Ticket {
	a := new(Ticket)
	a.CGI = a
	a.Base = base
	a.Uri = uri
	a.Provider = provider
	return a
}

func (self *Ticket) Handler() error {
	ARGS := self.R.Form
	found, err := self.R.Cookie(self.C.Go_probe_name)
	if (err == nil && self.Uri == "") {
		if (found == nil) { return Err(404) }
		self.Uri = found.Value
	}

	role := self.C.Roles[self.Role_value]
	issuer := role.Issuers[self.Provider]
	if (ARGS.Get(issuer.Credential[2]) != "") { // direct login
		return self.Handler_login()
	}

	if (err != nil || found == nil) {
		self.Set_cookie_session(self.C.Go_probe_name, self.Uri)
		self.Send_nocache(self.Login_page(1036))
		return nil
	}

	if (ARGS.Get(self.C.Go_err_name) != "") {
		erri, _ := strconv.Atoi(ARGS.Get(self.C.Go_err_name))
		self.Send_nocache(self.Login_page(erri))
		return nil
	}

	return self.Handler_login()
}

func (self *Ticket)Handler_login() error {
	ARGS := self.R.Form
	role := self.C.Roles[self.Role_value]
	issuer := role.Issuers[self.Provider]

	if (ARGS.Get(role.Surface) != "") {
		passin := ARGS.Get(role.Surface)
		if (passin != "" && role.Surface==issuer.Credential[3]) {
			err := self.Verify_cookie(passin)
			if (err != nil) {
				self.Send_nocache(self.Login_page(err.(Gerror).Code))
				return nil
			} else {
				self.Set_cookie(role.Surface, passin, role.Max_age)
				self.Set_cookie_session(role.Surface+"_", passin)
				return Err(303, self.Uri)
			}
		}
	}

	login    := ARGS.Get(issuer.Credential[0])
	password := ARGS.Get(issuer.Credential[1])
	err := self.CGI.Authenticate(login, password, self.Uri)
	if (err != nil) {
		if (err.(Gerror).Code < 1000) {
			return err
		} else {
			self.Send_nocache(self.Login_page(err.(Gerror).Code))
			return nil
		}
	}

	return self.Handler_fields()
}

func (self *Ticket)Handler_fields() error {
	role := self.C.Roles[self.Role_value]
	fields := make([]string, len(role.Attributes))
	for i, v := range role.Attributes {
		out := self.Out_hash[v]
		if (out == nil) { continue }
		switch out.(type) {
		case []uint8:
			fields[i] = string(out.([]uint8))
		case int32:
			fields[i] = strconv.Itoa(int(out.(int32)))
		case uint32:
			fields[i] = strconv.Itoa(int(out.(uint32)))
		case int64:
			fields[i] = strconv.Itoa(int(out.(int64)))
		case uint64:
			fields[i] = strconv.Itoa(int(out.(uint64)))
		default:
			fields[i] = out.(string)
		}
	}

	if (fields[0]=="") { return Err(1032) }
	signed := self.Signature(fields...)
	self.Set_cookie(role.Surface, signed, role.Max_age)
	self.Set_cookie_session(role.Surface+"_", signed)

	chartag, ok := self.C.Chartags[self.Chartag_value]
	if (ok && chartag.Case > 0) {
		self.Send_nocache(chartag.Call_logged())
		return nil
	}

	return Err(303, self.Uri)
}

func (self *Ticket)Authenticate(login, password, uri string) error {
	if (login=="" || password=="") { return Err(1037) }
	role := self.C.Roles[self.Role_value]
	issuer := role.Issuers[self.Provider]
	if (login != issuer.Provider_pars["Def_login"] || password != issuer.Provider_pars["Def_password"]) {
		return Err(1031)
	}

	role.Attributes = []string{"login","provider"}
	self.Out_hash = map[string]interface{}{"login":issuer.Provider_pars["Def_login"],"provider":self.Provider}

	return nil
}

func (self *Ticket) Login_page(erri int) string {
	role := self.C.Roles[self.Role_value]
	issuer := role.Issuers[self.Provider]
	chartag := self.C.Chartags[self.Chartag_value]
	if (chartag.Case > 0) {
		return chartag.Call_failed()
	}

	disk := self.C.Template+"/"+self.Role_value+"/"+self.C.Login_name+"."+self.Chartag_value
	T, err :=  template.ParseFiles(disk)
	if (err != nil) {
		page := `<html><head><title>Sign In</title><META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8"></head><body><h2>Genelet Login</h2><h3>{{.Other.Errorstr}}</h3>
{{if .Other.go_uri}}<FORM METHOD="POST" ACTION="` + self.C.Login_name + `"><INPUT TYPE="HIDDEN" NAME="{{.Other.Go_uri_name}}" VALUE="{{.Other.Uri}}"><p>Login Name<INPUT class=contact TYPE="TEXT" NAME="{{.Other.Login}}" /></p><p>Password<INPUT class=contact TYPE="PASSWORD" NAME="{{.Other.Password}}" /></p><p><INPUT class="submit" TYPE="SUBMIT" VALUE=" Sign In " /></p></FORM>
{{end}}</body></html>`
		T = template.Must(template.New("loginpage").Parse(page))
	}

	login := new(Tmpl)
	login.Success = true
	login.File = disk
	login.Other = map[string]interface{}{"Errorstr":Err(erri).Error(), "Login_name":self.C.Login_name, "Provider_name":self.C.Provider_name, self.C.Role_name:self.Role_value, self.C.Tag_name:self.Chartag_value, "Go_uri_name":self.C.Go_uri_name, "Uri":self.Uri, self.C.Go_uri_name:self.Uri, "Login":issuer.Credential[0], "Password":issuer.Credential[1]}
	str := ""
	err = login.Get_page(&str, T)
	if (err != nil) { return "1800: " + err.Error() }
	return str
}
