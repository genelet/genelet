package genelet

// In_pars and Out_pars must match exactly what needed in procedure and 
// the names in Out_pars should follow those in Attributes otherwise it
// will not be reagconized
import (
//"log"
	"regexp"
	"net/url"
	"database/sql"
)

type Procedure struct {
	DBH *sql.DB
	Ticket
}

func New_Procedure(base Base, db *sql.DB, uri string, provider string) *Procedure {
    a := new(Procedure)
    a.CGI = a
    a.Base = base
    a.DBH = db
	a.Uri = uri
	a.Provider = provider
	return a
}

func (self *Procedure)Run_sql(call_name string, in_vals []interface{}) error {
	role := self.C.Roles[self.Role_value]
	issuer := role.Issuers[self.Provider]
	out_pars := issuer.Out_pars
	if (out_pars == nil) {out_pars = role.Attributes}

	matched, err := regexp.MatchString("select", call_name)
	if (err != nil) { return Err(1034,err.Error()) }

	dbi := &DBI{DBH:self.DBH}

	self.Out_hash = make(map[string]interface{})
	var ret error
	if (matched) {
		ret = dbi.Get_sql_label(self.Out_hash, call_name, out_pars, in_vals...)
	} else {
//log.Printf("%v\n", out_pars);
//log.Printf("%v\n", call_name);
//log.Printf("%v\n", in_vals);
		ret = dbi.Do_proc(self.Out_hash, out_pars, call_name, in_vals...)
	}
	if (ret != nil) { return Err(1036, ret.Error()) }
	par, ok := self.Out_hash[out_pars[0]]
	if (!ok || par==nil) { return Err(1031) }
	return nil
}

func (self *Procedure) Authenticate(login, password, uri string) error {
	role := self.C.Roles[self.Role_value]
	issuer := role.Issuers[self.Provider]
	in_vals := []interface{}{login, password}

	if ((issuer.Screen & 1) !=0) {in_vals = append(in_vals, uri)}
	if ((issuer.Screen & 2) !=0) {in_vals = append(in_vals, Ip2int(self.Get_ip()))}
//	if ((issuer.Screen & 3) !=0) {in_vals = append(in_vals, self.Get_ua())}
//	if ((issuer.Screen & 4) !=0) {in_vals = append(in_vals, self.Get_referer())}

	ret := self.Run_sql(issuer.Sql, in_vals)
    if (ret !=nil) {return ret}

	par, ok := self.Out_hash[role.Attributes[0]]
	if (!ok || par==nil) { return Err(1032) }
	return nil
}

func (self *Procedure) Authenticate_as(login interface{}) error {
	in_vals := []interface{}{login}

	role := self.C.Roles[self.Role_value]
	issuer := role.Issuers[self.Provider]
	ret := self.Run_sql(issuer.Sql_as, in_vals)
    if (ret !=nil) {return ret}

	par, ok := self.Out_hash[role.Attributes[0]]
	if (!ok || par==nil) { return Err(1032) }
	return nil
}

func (self *Procedure)Callback_address(uri string) string {
	http := "http"
	if (self.R.TLS != nil) {
		http += "s"
	}
	return http + "://" + self.R.Host + self.C.Script_name + "/" + self.Role_value + "/" + self.Chartag_value + "/" + self.Provider + "?" + self.C.Go_uri_name + "=" + url.QueryEscape(uri)
}

func (self *Procedure)Fill_provider(back map[string]interface{}, uri string) error {
	role := self.C.Roles[self.Role_value]
    issuer := role.Issuers[self.Provider]

	in_vals := make([]interface{},0)
	for _, par := range issuer.In_pars {
      val, ok := back[par]
      if !ok { return Err(1144) }
      in_vals = append(in_vals, val)
    }
	if ((issuer.Screen & 1) !=0) {in_vals = append(in_vals, uri)}
	if ((issuer.Screen & 2) !=0) {in_vals = append(in_vals, Ip2int(self.Get_ip()))}

//log.Printf("10 %#v\n", issuer.Sql);
    err := self.Run_sql(issuer.Sql, in_vals)
//log.Printf("11 %#v\n", in_vals);
    if (err != nil) { return err }

//log.Printf("12 %#v\n", role.Attributes);
//log.Printf("13 %#v\n", self.Out_hash);
//log.Printf("14 %#v\n", back);
	for _, v := range role.Attributes {
		_, ok := self.Out_hash[v]
		if (!ok) {
			self.Out_hash[v] = back[v]
		}
	}
//log.Printf("15 %#v\n", self.Out_hash);
	par, ok := self.Out_hash[role.Attributes[0]]
	if (!ok || par==nil) { return Err(1032) }

	return nil
}
