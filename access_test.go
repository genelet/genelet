package genelet;

import (
    "testing"
    "bytes"
	"net/http"
	"net/http/httptest"
)

type TAccess struct {
	Access
}
func (self *TAccess)Set_ip() string {
    return "123.123.123.123"
}
func (self *TAccess)Set_when() int {
    return 1
}
func New_TAccess(base Base) *TAccess {
	a := new(TAccess)
	a.CGI = a
	a.Base = base
	return a
}

func TestAccess(t *testing.T) {
	configure := New_Config(filename)
	role := configure.Roles["m"]
	ac := role
	if ac.Surface !=  "mc" {
		t.Errorf("%s wanted", ac.Surface)
	}
	if ac.Coding !=  "jkfjowc29xgzymzg3dj5fq2w8z9vvrmb1owvnp9z4k96ttgx0r" {
		t.Errorf("%s wanted", ac.Coding)
	}
	if ac.Secret !=  "w76lvjnrsksaudhrde0ug3wv5k5nywj6ifv5zvy5qjet94po5j" {
		t.Errorf("%s wanted", ac.Secret)
	}
	if ac.Domain != "genelet.com" {
		t.Errorf("%s wanted", ac.Domain)
	}
	if ac.Duration != 360000 {
		t.Errorf("%d wanted", ac.Duration)
	}
	if ac.Userlist[0] != "x1" {
		t.Errorf("%d wanted", ac.Userlist[0])
	}
	if ac.Userlist[2] != "x3" {
		t.Errorf("%d wanted", ac.Userlist[2])
	}
	if ac.Grouplist != nil {
		t.Errorf("%v wanted", ac.Grouplist)
	}

	r, err := http.NewRequest("GET", "http://xxx.yyy", bytes.NewBuffer([]byte("sss")))
	if err != nil {
		panic(err)
	}
	w := httptest.NewRecorder()
	base := new_Base(configure, "m", "json", w, r)
	access := New_TAccess(*base)

	sig := access.Signature("x2","g1","g2","g3")
	ret := access.Verify_cookie(sig) // within 1 second
	if ret != nil {
		t.Errorf("%s wanted", ret.Error())
	}
	if r.Header["X-Forwarded-User"][0] != "x2" {
		t.Errorf("%s wanted", "x2")
	}
	if r.Header["X-Forwarded-Group"][0] != "g1|g2|g3" {
		t.Errorf("%s wanted", "g1|g2|g3")
	}
	if r.Header["X-Forwarded-Time"][0] != "360001" {
		t.Errorf("%d wanted", 360001)
	}
	if r.Header["X-Forwarded-Duration"][0] != "360000" {
		t.Errorf("%d wanted", 360000)
	}
	if r.Header["X-Forwarded-Request_time"][0] != "1" {
		t.Errorf("%d wanted", 1)
	}
	if r.Header["X-Forwarded-Raw"][0] != "Ec9rwEEzh1/0UTuoE7dvi+lv1yvnF2niRA2N4yYNbq3RoCp4b/Par3EUdNGGqUwsHfg1O6jtk3IvLaPSuW8/" {
		t.Errorf("%d wanted", "Ec9rwEEzh1/0UTuoE7dvi+lv1yvnF2niRA2N4yYNbq3RoCp4b/Par3EUdNGGqUwsHfg1O6jtk3IvLaPSuW8/")
	}
	if r.Header["X-Forwarded-Hash"][0] != "M2vfsmw5A|kDPI8K63ImKw-0uBE_" {
		t.Errorf("%d wanted", "M2vfsmw5A|kDPI8K63ImKw-0uBE_")
	}

	sig = access.Signature("x3","g2","g3","g4")
	ret = access.Verify_cookie(sig) // within 1 second
	if ret != nil {
		t.Errorf("%s wanted", ret.Error())
	}
	if r.Header["X-Forwarded-User"][0] != "x2" {
		t.Errorf("%s wanted", "x2")
	}
	if r.Header["X-Forwarded-User"][1] != "x3" {
		t.Errorf("%s wanted", "x3")
	}
	if r.Header["X-Forwarded-Group"][0] != "g1|g2|g3" {
		t.Errorf("%s wanted", "g1|g2|g3")
	}
	if r.Header["X-Forwarded-Group"][1] != "g2|g3|g4" {
		t.Errorf("%s wanted", "g2|g3|g4")
	}

	sig = access.Signature("bad_guy","g2","g3","g4")
	ret = access.Verify_cookie(sig) // within 1 second
	if ret.(Gerror).Code != 1021 {
		t.Errorf("%s wanted", ret.Error())
	}

	access.Send_nocache("okok")
    h := w.Header()
    if (h.Get("Cache-Control") != "no-cache, no-store, max-age=0, must-revalidate") {
        t.Errorf("%s gotten",h.Get("Cache-Control"))
    }
    if (h.Get("Pragma") != "no-cache") {
        t.Errorf("%s gotten",h.Get("Pragma"))
    }
	if (w.Body.String() != "okok") {
        t.Errorf("%s wanted", "okok")
    }
}
