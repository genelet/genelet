package genelet;

import (
	"os"
	"log"
	"time"
)

type Loggertype int

const (
	l_emergency Loggertype = iota
	l_alert
	l_critical
	l_warn
	l_notice
	l_info
	l_debug
)

var (
	logging_names = map[string]Loggertype{"Emergency":l_emergency, "Alert":l_alert, "Critical":l_critical, "Warn":l_warn, "Notice":l_notice, "Info":l_info, "Debug":l_debug}
)

type Logging struct {
	log.Logger
	Filename string
	Minlevel string
	Maxlevel string
}

func New_Logging(filename, minlevel, maxlevel string) *Logging {
    f, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
    if err != nil { return nil }

	old := log.New(f, "", log.Lshortfile)
	return &Logging{*old, filename, minlevel, maxlevel}
}

func (self *Logging) Screen_start(method, uri, ip, ua string)  {
	self.Warn("GENELET LOGGER {New Screen}{", time.Now(), "}{", ip, "}{", method, "}", uri, ua)
	return
}

func (self *Logging) log_(level string, msg ...interface{}) {
	if self.Is_log(level)==false { return }

	for _, v := range msg {
		switch v.(type) {
		case string:
			self.Printf("[%s %d]%s\n", level, os.Getpid(), v.(string))
		default:
			self.Printf("[%s %d]%v\n", level, os.Getpid(), v)
		}
	}
}

func (self *Logging) Debug(msg ...interface{}) {
	self.log_("Debug", msg ...)
}
func (self *Logging) Info(msg ...interface{}) {
	self.log_("Info", msg ...)
}
func (self *Logging) Notice(msg ...interface{}) {
	self.log_("Notice", msg ...)
}
func (self *Logging) Warn(msg ...interface{}) {
	self.log_("Warn", msg ...)
}
func (self *Logging) Critical(msg ...interface{}) {
	self.log_("Critical", msg ...)
}
func (self *Logging) Alert(msg ...interface{}) {
	self.log_("Alert", msg ...)
}
func (self *Logging) Emergency(msg ...interface{}) {
	self.log_("Emergency", msg ...)
}

func (self *Logging) Is_log(level string) bool {
	return (logging_names[level] >= logging_names[self.Minlevel] && logging_names[level] <= logging_names[self.Maxlevel])
}

func (self *Logging) Is_debug()     bool { return self.Is_log("Debug") }
func (self *Logging) Is_info()      bool { return self.Is_log("Info") }
func (self *Logging) Is_notice()    bool { return self.Is_log("Notice") }
func (self *Logging) Is_warn()      bool { return self.Is_log("Warn") }
func (self *Logging) Is_critical()  bool { return self.Is_log("Critical") }
func (self *Logging) Is_alert()     bool { return self.Is_log("Alert") }
func (self *Logging) Is_emergency() bool { return self.Is_log("Emergency") }
