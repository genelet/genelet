package genelet;

import (
	"testing"
    "database/sql"
	"log"
	"regexp"
	"net/http"
	"net/http/httptest"
)

type TProcedure struct {
    Procedure
}

func New_TProcedure(base Base, db *sql.DB, uri string, provider string) *TProcedure {
    a := new(TProcedure)
    a.CGI = a
    a.Base = base
    a.DBH = db
    a.Uri = uri
    a.Provider = provider
    return a
}
func (self *TProcedure)Set_ip() string {
    return "123.123.123.123"
}
func (self *TProcedure)Set_when() int {
    return 1
}

func TestDbiProcedure(t *testing.T) {
    configure := New_Config(filename)
	db, err := sql.Open("mysql", "eightran_goto:12pass34@/wavelet")

	req, err := http.NewRequest("GET", "http://example.com/foo?email=hello&passwd=world", nil)
	if err != nil {
		log.Fatal(err)
	}
	w := httptest.NewRecorder()
	b := new_Base(configure, "m", "json", w, req)

	tticket := New_TProcedure(*b, db, "http://xxx.yyy.zzz/foo/bar", "db")
	//issuer := tticket.C.Roles["m"].Issuers["db"]
	ret := tticket.Authenticate("hello","world","asw")
	if ret.(Gerror).Code != 1031 {
		t.Errorf("%s wrong login expected", ret.Error())
	}
	ret = tticket.Authenticate("a","b","asw")
	if ret != nil {
		t.Errorf("%s corrent login expected", ret.Error())
	}
	if tticket.Out_hash["m_id"].(int64) != 1 {
		t.Errorf("%d wanted", tticket.Out_hash["m_id"].(int64))
	}
	if string(tticket.Out_hash["first_name"].(string)) != "c" {
		t.Errorf("%s wanted", tticket.Out_hash["first_name"].(string))
	}
	ret = tticket.Handler_fields()
	if (ret != nil) {
		t.Errorf("%d returned", ret.Error())
	}

// test Handler with direct login
	req, _ = http.NewRequest("GET", "http://example.com/foo?email=a&passwd=b&direct=1", nil)
	w = httptest.NewRecorder()
	b = new_Base(configure, "m", "e", w, req)
	tticket = New_TProcedure(*b, db, "http://xxx.yyy.zzz/foo/bar", "db")
	_ = req.ParseForm()
	ret = tticket.Handler()
	if ret.(Gerror).Code != 303 {
		t.Errorf("%s wanted", ret.Error())
	}
	h := w.Header()
	matched, err := regexp.MatchString("^mc=Ec9rwEEzh1\\/0UTuoE7dvi\\/ByyTC1F2qsXRbY4yYNbq3RoCpNDcLkiEYmJdaLrz8uBYUQZe\\/dglVvVtXQsRI\\/", h.Get("Set-Cookie"))
	if !matched {
		t.Errorf("%s wanted", h.Get("Set-Cookie"))
	}
	db.Close()
}
